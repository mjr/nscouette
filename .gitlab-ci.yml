#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# This file is part of nsCouette -- A high-performance code for direct         !
# numerical simulations of turbulent Taylor-Couette flow                       !
#                                                                              !
# Copyright (C) 2019 Marc Avila, Bjoern Hof, Jose Manuel Lopez, Markus Rampp,  !
#                    Liang Shi, Alberto Vela-Martin, Daniel Feldmann.          !
#                                                                              !
# nsCouette is free software: you can redistribute it and/or modify it under   !
# the terms of the GNU General Public License as published by the Free         !
# Software Foundation, either version 3 of the License, or (at your option)    !
# any later version.                                                           !
#                                                                              !
# nsCouette is distributed in the hope that it will be useful, but WITHOUT ANY !
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS    !
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more        !
# details.                                                                     !
#                                                                              !
# You should have received a copy of the GNU General Public License along with !
# nsCouette. If not, see <http://www.gnu.org/licenses/>.                       !
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#default image for all jobs
image: gitlab-registry.mpcdf.mpg.de/mpcdf/ci-module-image/gcc_13:2024


stages:
  - build
  - verify 
  - deploy
 
build-TEcode_gcc-mkl:
  stage: build
  image: gitlab-registry.mpcdf.mpg.de/mpcdf/ci-module-image/gcc_13-impi_2021_9:2024
  before_script:
    - module load anaconda/3
    - pip install 'gcovr<7.0'  
  script: 
    - module load gcc impi mkl hdf5-mpi
    - make ARCH=gcc-mkl CODE=TE_CODE HDF5IO=yes PROFLIB=FTIM DEBUG=yes
    -  sed -e 's/m_r\s*=\s*\([0-9]\+\)/m_r = 27/' -e  's/m_th\s*=\s*\([0-9]\+\)/m_th = 16/' -e 's/m_z0\s*=\s*\([0-9]\+\)/m_z0 = 32/' -e 's/numsteps\s*=\s*\([0-9]\+\)/numsteps = 200/' -e 's/dn_hdf5\s*=\s*\([0-9]\+\)/dn_hdf5 = 100/' input_nsCouette > input_nsCouette.test_short 
    - .gitlab-ci/test/run_nscouette.sh gcc-mkl 3 2
    - module load anaconda/3
    - export PATH=/root/.local/bin:${PATH}
    - cd gcc-mkl
    - gcovr -r .. --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml
    - gcovr -r .. --html-details coverage.html
  coverage: /^\s*lines:\s*\d+.\d+\%/
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: gcc-mkl/coverage.xml
    paths:
      - gcc-mkl/coverage.*
    expire_in: 1 hour
    when: always
  cache:
    key: "pip_cache_20240930"
    paths:
       - .cache/pip
  variables:
      PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  tags: 
    - docker

build-STDcode_gcc-mkl:
  stage: build
  image: gitlab-registry.mpcdf.mpg.de/mpcdf/ci-module-image/gcc_13-impi_2021_9:2024
  script: 
#    - ".gitlab-ci/build/build_nscouette.sh STD_CODE gcc-mkl DEBUGyes"
    - module load gcc impi mkl hdf5-mpi
    - make ARCH=gcc-mkl CODE=STD_CODE HDF5IO=yes PROFLIB=FTIM DEBUG=yes
    - sed -e 's/m_r\s*=\s*\([0-9]\+\)/m_r = 27/' -e  's/m_th\s*=\s*\([0-9]\+\)/m_th = 16/' -e 's/m_z0\s*=\s*\([0-9]\+\)/m_z0 = 32/' -e 's/numsteps\s*=\s*\([0-9]\+\)/numsteps = 200/' -e 's/dn_hdf5\s*=\s*\([0-9]\+\)/dn_hdf5 = 100/' input_nsCouette > input_nsCouette.test_short 
    - .gitlab-ci/test/run_nscouette.sh gcc-mkl 3 2
  tags: 
    - docker


build-TEcode_intel-mkl:
  stage: build
  image: gitlab-registry.mpcdf.mpg.de/mpcdf/ci-module-image/intel_2024_0-impi_2021_11:2024
  script: 
    - module load intel impi mkl hdf5-mpi
    - make ARCH=intel-mkl CC=icx CODE=TE_CODE HDF5IO=yes PROFLIB=NONE DEBUG=yes
    - sed -e 's/m_r\s*=\s*\([0-9]\+\)/m_r = 27/' -e  's/m_th\s*=\s*\([0-9]\+\)/m_th = 16/' -e 's/m_z0\s*=\s*\([0-9]\+\)/m_z0 = 32/' -e 's/numsteps\s*=\s*\([0-9]\+\)/numsteps = 200/' -e 's/dn_hdf5\s*=\s*\([0-9]\+\)/dn_hdf5 = 100/' input_nsCouette > input_nsCouette.test_short 
    - .gitlab-ci/test/run_nscouette.sh intel-mkl 3 2
  tags: 
    - docker


build-STDcode_intel-mkl:
  stage: build
  image: gitlab-registry.mpcdf.mpg.de/mpcdf/ci-module-image/intel_2024_0-impi_2021_11:2024
  script: 
    - module load intel impi mkl hdf5-mpi
    - make ARCH=intel-mkl CC=icx CODE=STD_CODE HDF5IO=yes PROFLIB=NONE DEBUG=yes
    -  sed -e 's/m_r\s*=\s*\([0-9]\+\)/m_r = 32/' -e  's/m_th\s*=\s*\([0-9]\+\)/m_th = 16/' -e 's/m_z0\s*=\s*\([0-9]\+\)/m_z0 = 32/' -e 's/numsteps\s*=\s*\([0-9]\+\)/numsteps = 200/' -e 's/dn_hdf5\s*=\s*\([0-9]\+\)/dn_hdf5 = 100/' input_nsCouette > input_nsCouette.test_short 
    - .gitlab-ci/test/run_nscouette.sh intel-mkl 4 2
  tags: 
    - docker


build-ford:
  stage: build
  image: gitlab-registry.mpcdf.mpg.de/mpcdf/ci-module-image/intel_2024_0-impi_2021_11:2024
  script:
    - module load anaconda/3/2023.03
    - pip install --user importlib-metadata==4.4 ford
    - export PATH=${PATH}:${HOME}/.local/bin
    - module load intel impi mkl
    - make CODE=TE_CODE HDF5IO=no ford 
  artifacts:
    paths:
    - ford-doc
  tags: 
    - docker

verify-STDcode_intel-mkl:
  stage: verify
  image: gitlab-registry.mpcdf.mpg.de/mpcdf/ci-module-image/intel_2024_0-impi_2021_11:2024
  script: 
    - module load intel impi mkl hdf5-mpi
    - make ARCH=intel-mkl CC=icx CODE=STD_CODE HDF5IO=yes PROFLIB=FTIM
    - .gitlab-ci/test/verify_nscouette.sh intel-mkl 4 2 reference_run_1
  resource_group: large_run
  artifacts:
    paths:
    - environment
    - nscouette.out
    - torque
  only: 
  tags: 
    - docker

verify-TEcode_intel-mkl:
  stage: verify
  image: gitlab-registry.mpcdf.mpg.de/mpcdf/ci-module-image/intel_2024_0-impi_2021_11:2024
  script: 
    - module load intel impi mkl hdf5-mpi
    - make ARCH=intel-mkl CC=icx CODE=TE_CODE HDF5IO=yes PROFLIB=FTIM
    - .gitlab-ci/test/verify_nscouette.sh intel-mkl 4 2 reference_run_2
  resource_group: large_run
  artifacts:
    paths:
    - environment
    - nscouette.out
    - torque
    - Nusselt
  only: 
  tags: 
    - docker

compareMPI-TEcode_intel-mkl:
  stage: verify
  image: gitlab-registry.mpcdf.mpg.de/mpcdf/ci-module-image/intel_2024_0-impi_2021_11:2024
  script: 
    - module load intel impi mkl hdf5-mpi
    - make ARCH=intel-mkl CC=icx CODE=TE_CODE HDF5IO=yes PROFLIB=FTIM
    -  sed -e 's/m_r\s*=\s*\([0-9]\+\)/m_r = 16/' -e  's/m_th\s*=\s*\([0-9]\+\)/m_th = 24/' -e 's/m_z0\s*=\s*\([0-9]\+\)/m_z0 = 18/' -e 's/numsteps\s*=\s*\([0-9]\+\)/numsteps = 200/' -e 's/dn_hdf5\s*=\s*\([0-9]\+\)/dn_hdf5 = 100/' input_nsCouette > input_nsCouette.test_short 
    - .gitlab-ci/test/run_compare.sh intel-mkl 4 2 8 1
  resource_group: large_run
  only: 
  tags: 
    - docker

validate-STDcode_intel-mkl:
  stage: verify
  image: gitlab-registry.mpcdf.mpg.de/mpcdf/ci-module-image/intel_2024_0-impi_2021_11:2024
  script: 
    - module load intel impi mkl
    - .gitlab-ci/test/validate_nscouette.sh intel-mkl 4 2 
  resource_group: large_run
  artifacts:
    paths:
    - environment
    - nscouette.out
    - wavespeed.out
    - parameter
  only: 
  tags: 
    - docker
 
pages:
  stage: deploy
  image: python:3.9
  tags:
    - cloud
  needs:
    - job: build-TEcode_gcc-mkl
    - job: build-ford
  script:
      - mkdir -p public
      - mv gcc-mkl/coverage.* public/
      - mv ford-doc/ public/
  artifacts:
      paths:
        - public
      expire_in: 60 days
  only:
    - master

