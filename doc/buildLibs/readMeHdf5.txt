

# 1. Download and unpack to some directory of your choice, eg ~/hdf5/build/.
mkdir -p $HOME/hdf5/build
cd $HOME/hdf5/build
cp $HOME/Downloads/hdf5-1.10.6.tar.gz .
tar xvf hdf5-1.10.6.tar.gz
cd hdf5-1.10.6

# 2. Load Intel compiler suite
module purge
module load Intel/PSXE2018

# 3. Check compiler version
module list
mpiicc --version
mpiifort --version

# 4. Define parallel Intel compilers to be used
export CC=mpiicc
export CCP="mpiicc -E"
export FC=mpiifort

# 5. Set compiler flags for larger file size support and also optimisation level 3
export CFLAGS="-O3 -mcmodel=medium -xHost -ip"
export FCFLAGS="-O3 -mcmodel=medium -xHost -ip"

# 6. Specify path to locally installed zlib library
export LIBS="-lz"
export LDFLAGS="-L/home/feldmann/zlib/zlib-1.2.11/lib"
export CPPFLAGS="-I/home/feldmann/zlib/zlib-1.2.11/include/"

# 7. Configure HDF5 to be build for parallel use with fortran interface, as well as
# the use of the local installation of zlib and to be installed into some
# place you wish, eg as follows
mkdir $HOME/hdf5/hdf5-1.10.6
./configure --enable-parallel --enable-fortran --with-zlib=/home/feldmann/zlib/zlib-1.2.11 --prefix=/home/feldmann/hdf5/hdf5-1.10.6

# 8. Build, test and install HDF5 as follows, while building and testing take quite
# some time... (enough for lunch and/or coffee)
make && make check
make install

# 9. Do not forget to unset environment varibles, what might mess up future
# builds in the same terminal
unset CC CPP FC
unset CFLAGS FCFLAGS CPPFLAGS LDFLAGS LIBS

# 10. You may want to add the path to the newly installed library to the
# LD_LIBRARY_PATH environment variable if that lib directory is not searched by
# default. Eg put the following line to your ~/.bashrc
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/feldmann/hdf5/hdf5-1.10.6/lib

# Done!



# This is a rough guideline of how to manually build MPI-parallel HDF5
# libraries with Fortran interfaces using gcc on a standard Linux system, where
# you have sudo rights available but no environment modules. This requires zlib,
# as described above.
# Daniel Feldmann, 6th February 2017

# 1. Download and unpack to some directory of your choice, eg ~/hdf5/build/.
mkdir -p $HOME/hdf5/build
cd $HOME/hdf5/build
cp $HOME/Downloads/hdf5-1.10.6.tar.gz .
tar xvf hdf5-1.10.6.tar.gz
cd hdf5-1.10.6

# 2. Make sure zlib version 1.2.5 or later is installed (do not confuse with
# libzc library), otherwise install it
ls /*/*zlib* /*/*/*zlib*
echo $LD_LIBRARY_PATH | grep --color=auto zlib

# 3. Specify path to locally installed zlib library
export LIBS="-lz"
export LDFLAGS="-L/home/feldmann/zlib/zlib-1.2.11/lib"
export CPPFLAGS="-I/home/feldmann/zlib/zlib-1.2.11/include/"

# 4. Install openmpi and GNU C compiler if not already installed, both easily
# available via the package manager (e.g. apt-get)
sudo apt-get install openmpi-bin libopenmpi-dev mpi-default-bin mpi-default-dev
sudo apt-get install gfortran gcc g++

# 5. Check GNU C and Fortran compiler versions, otherwise install
which mpicc
mpicc --version
which mpif90
mpif90 --version

# 6. Set GNU C and Fortran compiler, parallel (openmpi) versions
export CC=mpicc
export CCP="mpicc -E"
export FC=mpif90

# 7. Set compiler flags for larger file size support and also optimisation
# level 3 for the fortran compiler
export CFLAGS="-mcmodel=medium -O3"
export FCFLAGS="-mcmodel=medium -O3"

# 8. Configure HDF5 to be build for parallel use with fortran interface, as well
# as the use of the local installation of zlib and to be installed into some
# place you wish, eg as follows
mkdir $HOME/hdf5/hdf5-1.10.6
./configure --enable-parallel --enable-fortran --with-zlib=/home/feldmann/zlib/zlib-1.2.11 --prefix=/home/feldmann/hdf5/hdf5-1.10.6

# 9. Build, test and install HDF5 as follows, while building and testing take quite
# some time... (enough for lunch and/or coffee). Note that the parallel tests
# will only succede on a parallel file system
make && make check
make install

# 10. Do not forget to unset environment varibles, what might mess up future
# builds
unset CC FC
unset CFLAGS FCFLAGS

# 11. You may need to add the path to the newly installed library to the
# LD_LIBRARY_PATH environment variable if that lib directory is not searched by
# default. \eg put the following line to your ~/.bashrc
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/feldmann/hdf5/hdf5-1.10.6/lib

# Done!
