# Brief comments on how to install zlib locally on our small institute Linux
# cluster (fsmcluster@zarm.uni-bremen.de), which is needed for HDF5.
# Daniel Feldmann, 18th January 2017

# Download and unpack to some directory of your choice, \eg ~/zlib/build
tar xfvz zlib-1.2.11.tar.gz
cd zlib-1.2.11/

# Load Intel compiler suite available on fsmclsuter
module purge
module load Intel/PSXE2018

# Check compiler version
module list
mpiicc --version

# Use Intel C compiler for the following steps, whatever icc or mpiicc, is not
# important as far as I know...
# export CC=icc
export CC=mpiicc
export CFLAGS="-O3 -xHost -ip -mcmodel=medium"

# Configure zlib to be installed into some place of you wish, \eg as follows
mkdir $HOME/zlib/zlib-1.2.11
./configure --prefix=/home/feldmann/zlib/zlib-1.2.11

# Build, test and install zlib via
make && make check && make install

# Do not forget to unset environment varibles, which might mess up future builds
unset CC
unset CFLAGS

# You may need to add the path to the newly installed library to the
# LD_LIBRARY_PATH environment variable if that lib directory is not searched by
# default. \eg put the following line to your ~/.bashrc
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/feldmann/zlib/zlib-1.2.11/lib

# Done!



# Brief comments on how to install zlib locally on a standard Linux system
# (darkstar@zarm.uni-bremen.de), which is needed for HDF5.
# Daniel Feldmann, 6th January 2017

# 1. Download and unpack to some directory of your choice, e.g. ~/zlib/build/.
mkdir -p $HOME/zlib/build
cd $HOME/zlib/build
cp $HOME/Downloads/zlib-1.2.11.tar.gz .
tar xfvz zlib-1.2.11.tar.gz
cd zlib-1.2.11

# 2. Install openmpi and GNU C compiler if not already installed, both easily
# available via the package manager (e.g. apt-get)
sudo apt-get install openmpi-bin libopenmpi-dev mpi-default-bin mpi-default-dev
sudo apt-get install gfortran gcc g++

# 3. Check compiler version
which mpicc
mpicc --version

# 4. Use GNU C compiler for the following steps, whatever gcc or mpicc, is not
# important as far as I know...
export CC=gcc
export CC=mpicc
export CFLAGS='-O3 -mcmodel=medium'

# 5. Configure zlib to be installed into some place you wish, eg as follows
mkdir $HOME/zlib/zlib-1.2.11
./configure --prefix=$HOME/zlib/zlib-1.2.11

# 6. Build, test and install zlib via
make && make check && make install

# 7. Do not forget to unset environment varibles, which might mess up subsequent
# builds in the same terminal.
unset CC
unset CFLAGS

# 8. You may need to add the path to the newly installed library to the
# LD_LIBRARY_PATH environment variable if that lib directory is not searched by
# default. Eg put the following line to your ~/.bashrc
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/feldmann/zlib/zlib-1.2.11/lib

# 9. Verify
echo $LD_LIBRARY_PATH | grep --color=auto zlib

# Done!
