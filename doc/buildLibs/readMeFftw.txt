# Brief comments on how to install fftw locally on a standard Linux Mint system
# (hal@zarm.uni-bremen.de) for use with MPI and OpenMP capabilities.
# Daniel Feldmann, 24th January 2020

# 1. Download and unpack to some directory of your choice, e.g. ~/fftw/build/.
mkdir -p $HOME/fftw/build/
cd $HOME/fftw/build/
cp $HOME/Downloads/fftw-3.3.8.tar.gz .
tar xfvz fftw-3.3.8.tar.gz
cd fftw-3.3.8

# 2. Check C and fortran compiler version
which mpicc
mpicc --version
which mpifort
mpifort --version

# 3. Define parallel Intel compilers to be used
export CC=mpicc
export MPICC=mpicc
export F77=mpifort

# 4. Set compiler flags for larger file size support and also optimisation
# level 3 for the Fortran compiler, also provide path to special MPI libs to be
# used, if required
export CFLAGS="-mcmodel=medium"
export FFLAGS="-mcmodel=medium -O3"

# 5. Configure fftw to be build with shared memory (threads and openMP) as well
# as distributed memory (mpi) parallelisation, also with shared libraries, and
# to be installed into some place you wish, e.g. as follows
mkdir -p $HOME/fftw/fftw-3.3.8
./configure --enable-fortran --enable-threads --enable-openmp --enable-mpi --enable-shared --prefix=/home/feldmann/fftw/fftw-3.3.8

# 6. Build, test and install fftw3 as follows, while building takes quite some
# time... enough for a coffee!
make && make check && make install

# 8. Do not forget to unset environment varibles, what might mess up future
# builds in the same terminal
unset CC MPICC F77
unset LDFLAGS CPPFLAGS

# 9. You may need to add the path to the newly installed library to your
# LD_LIBRARY_PATH environment variable if that lib directory is not searched by
# default. E.g. put the following line to your ~/.bashrc
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/feldmann/fftw/fftw-3.3.8/lib

# Done!
