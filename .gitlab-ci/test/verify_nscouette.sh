#!/bin/bash

################################################################################
# This file is part of nsCouette -- A high-performance code for direct         #
# numerical simulations of turbulent Taylor-Couette flow                       #
#                                                                              #
# Copyright (C) 2019 Marc Avila, Bjoern Hof, Jose Manuel Lopez, Markus Rampp,  #
#                    Liang Shi, Alberto Vela-Martin, Daniel Feldmann.          #
#                                                                              #
# nsCouette is free software: you can redistribute it and/or modify it under   #
# the terms of the GNU General Public License as published by the Free         #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# nsCouette is distributed in the hope that it will be useful, but WITHOUT ANY #
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS    #
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more        #
# details.                                                                     #
#                                                                              #
# You should have received a copy of the GNU General Public License along with #
# nsCouette. If not, see <http://www.gnu.org/licenses/>.                       #
################################################################################



set -e
function exittrap {
    echo "$0 failed"
    exit 1
}
trap exittrap ERR INT TERM

ARCH=$1
NUMPROCS=${2:-4}
NUMTHRDS=${3:-2}
REFCASE=${4:reference_run_1}

#run the code
#module load intel/19.1.3 impi/2019.9

export OMP_NUM_THREADS=$NUMTHRDS
export OMP_PLACES=cores
export OMP_STACKSIZE=128M
ulimit -s unlimited

mpiexec -n $NUMPROCS $ARCH/nsCouette.x < .gitlab-ci/test/$REFCASE/input_nsCouette > nscouette.out

cat /proc/cpuinfo > environment 
env >> environment

#compare results
curl http://download-mirror.savannah.gnu.org/releases/numdiff/numdiff-5.9.0.tar.gz --output numdiff-5.9.0.tar.gz
tar xzf numdiff-5.9.0.tar.gz
cd numdiff-5.9.0
./configure
make
make install

RES=.
REF=.gitlab-ci/test/$REFCASE

#informational output
FILES="ke_mode  ke_th  ke_total  ke_z  torque  Nusselt"
for file in $FILES; do
    if [ -e $REF/$file ]; then
        echo "@comparing $RES/$file $REF/$file" 
        numdiff --quiet --statistics $RES/$file $REF/$file || echo "info output" 
    fi
done

#verification
FILES="torque  Nusselt"
for file in $FILES; do
    if [ -e $REF/$file ]; then
        echo "@verifying $RES/$file $REF/$file" 
        numdiff --relative=2e-8 $RES/$file $REF/$file
    fi
done
