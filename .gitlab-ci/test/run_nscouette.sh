#!/bin/bash

################################################################################
# This file is part of nsCouette -- A high-performance code for direct         #
# numerical simulations of turbulent Taylor-Couette flow                       #
#                                                                              #
# Copyright (C) 2019 Marc Avila, Bjoern Hof, Jose Manuel Lopez, Markus Rampp,  #
#                    Liang Shi, Alberto Vela-Martin, Daniel Feldmann.          #
#                                                                              #
# nsCouette is free software: you can redistribute it and/or modify it under   #
# the terms of the GNU General Public License as published by the Free         #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# nsCouette is distributed in the hope that it will be useful, but WITHOUT ANY #
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS    #
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more        #
# details.                                                                     #
#                                                                              #
# You should have received a copy of the GNU General Public License along with #
# nsCouette. If not, see <http://www.gnu.org/licenses/>.                       #
################################################################################



set -e
function exittrap {
    echo "$0 failed"
    exit 1
}
trap exittrap ERR INT TERM

ARCH=$1
NUMPROCS=${2:-4}
NUMTHRDS=${3:-2}

#ITAC variables for check-mpi option, only Intel-MPI
if [ ! -z ${I_MPI_ROOT} ];
then
    export VT_MPI=impi4
    export VT_ADD_LIBS="-ldwarf -lelf -lvtunwind -lm -lpthread"
    export VT_ROOT="$I_MPI_ROOT/../../itac/latest/"
    export VT_LIB_DIR="$VT_ROOT/lib"
    export VT_SLIB_DIR="$VT_ROOT/slib"
    export LD_LIBRARY_PATH="$VT_ROOT/slib:$LD_LIBRARY_PATH"
    export CHECK_MPI="-check-mpi"
else
    export CHECK_MPI=""
fi

export OMP_NUM_THREADS=$NUMTHRDS
export OMP_PLACES=cores
export OMP_STACKSIZE=128M

mpiexec -n $NUMPROCS $CHECK_MPI $ARCH/nsCouette.x < input_nsCouette.test_short 


  
