#!/bin/bash

################################################################################
# This file is part of nsCouette -- A high-performance code for direct         #
# numerical simulations of turbulent Taylor-Couette flow                       #
#                                                                              #
# Copyright (C) 2019 Marc Avila, Bjoern Hof, Jose Manuel Lopez, Markus Rampp,  #
#                    Liang Shi, Alberto Vela-Martin, Daniel Feldmann.          #
#                                                                              #
# nsCouette is free software: you can redistribute it and/or modify it under   #
# the terms of the GNU General Public License as published by the Free         #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# nsCouette is distributed in the hope that it will be useful, but WITHOUT ANY #
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS    #
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more        #
# details.                                                                     #
#                                                                              #
# You should have received a copy of the GNU General Public License along with #
# nsCouette. If not, see <http://www.gnu.org/licenses/>.                       #
################################################################################



# compare the numerically computed wave speed with an experimentally
# determined value 0.34432 (King et al., J Fluid Mech, 1984)

set -e 
function exittrap {     
    echo "$0 failed"
    exit 1 
} 
trap exittrap ERR INT TERM


ARCHITECTURE=$1
NUMPROCS=${2:-4}
NUMTHRDS=${3:-2}


#build the code and postprocessing program
make CC=icx HDF5IO=no CODE=STD_CODE PROFLIB=FTIM ARCH=$ARCHITECTURE
make CC=icx HDF5IO=no CODE=STD_CODE PROFLIB=FTIM ARCH=$ARCHITECTURE postproc

#run the code
rm -f coeff_*.*

cat /proc/cpuinfo > environment 
env >> environment

ln -fs .gitlab-ci/test/validation_run_1/coeff_WSPEED.00000000
cp .gitlab-ci/test/validation_run_1/coeff_WSPEED.00000000.info restart

export OMP_NUM_THREADS=$NUMTHRDS
export OMP_PLACES=cores
export OMP_STACKSIZE=128M
ulimit -s unlimited

mpiexec -n $NUMPROCS $ARCHITECTURE/nsCouette.x < .gitlab-ci/test/validation_run_1/input_nsCouette > nscouette.out


#compute wave speed
$ARCHITECTURE/waveSpeed.x < .gitlab-ci/test/validation_run_1/input_waveSpeed > wavespeed.out

#validate results
wspeed=`grep '# non-dimensional wavespeed' wavespeed.out | awk -F'=' '{print $2}'`

wspeed_max=0.35
wspeed_min=0.33
echo "comparing computed wavespeed with experimentally determined value 0.34432"
if [ $wspeed \> $wspeed_min ] && [ $wspeed \< $wspeed_max ]; then
    echo "SUCCESS: wavespeed=$wspeed within bounds [$wspeed_min,$wspeed_max]"
    exit 0
else
    echo "FAILURE: wavespeed=$wspeed out of bounds [$wspeed_min,$wspeed_max]"
    exit 1
fi

