################################################################################
# This file is part of nsCouette -- A high-performance code for direct         #
# numerical simulations of turbulent Taylor-Couette flow                       #
#                                                                              #
# Copyright (C) 2019 Marc Avila, Bjoern Hof, Jose Manuel Lopez, Markus Rampp,  #
#                    Liang Shi, Alberto Vela-Martin, Daniel Feldmann.          #
#                                                                              #
# nsCouette is free software: you can redistribute it and/or modify it under   #
# the terms of the GNU General Public License as published by the Free         #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# nsCouette is distributed in the hope that it will be useful, but WITHOUT ANY #
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS    #
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more        #
# details.                                                                     #
#                                                                              #
# You should have received a copy of the GNU General Public License along with #
# nsCouette. If not, see <http://www.gnu.org/licenses/>.                       #
################################################################################



&parameters_grid
m_r   = 32                 ! radial points           => m_r      grid points (radial)
m_th  = 384                ! azimuthal Fourier modes => 2*m_th+1 grid points (azimuthal)
m_z0  = 320                ! axial Fourier modes     => 2*m_z0+1 grid points (axial)
k_th0 = 1.0d0              ! azimuthal wavenumber    => L_th = 2*pi/k_th0 azimuthal length of grid
k_z0  = 3.14159265358979d0 ! axial wavenumber        => L_z =  2*pi/k_z0  axial length of grid
eta   = 0.5d0              ! aspect ratio => r_i = eta/(1-eta),r_o = 1/(1-eta) inner/outer radius
alpha = 0.0d0              ! distribution of radial nodes, 0: Chebyshev, 1: uniform, <0: radial_distribution.in                      
/

&parameters_physics
Re_i = 111.1d0         ! Inner cylinder Reynolds number
Re_o =   0.0d0         ! Outer cylinder Reynolds number
Gr   = 280.0d0         ! Grashof number
Pr   =   15.d0         ! Prandtle number
gap  =    3.25d0       ! gap size in cm                          [TE_CODE only]
gra  =  980.0d0        ! gravitational acceleration in g/cm**3   [TE_CODE only]
nu   =    1.01d-2      ! kinematic viscosity in cm**2 /s         [TE_CODE only]
/

&parameters_timestep
numsteps    = 100       ! number of steps 
init_dt     = 1.0d-4    ! initial size of timestep
variable_dt = T         ! use a variable (=T) or fixed (=F) timestep
maxdt       = 1.0d-2    ! maximum size of timestep
Courant     = 0.25d0    ! CFL safety factor
/

&parameters_output
fBase_ic = 'REF_2'      ! identifier for coeff_ (checkpoint) and fields_ (hdf5) files 
dn_coeff = 5000         ! output interval [steps] for coeff (dn_coeff = -1 disables ouput)
dn_ke    = 10           ! output interval [steps] for energy
dn_vel   = 10           ! output interval [steps] for velocity
dn_Nu    = 10           ! output interval [steps] for Nusselt (torque)
dn_hdf5  = 1000         ! output interval [steps] for HDF5 output
print_time_screen = 10  ! output interval [steps] for timestep info to stdout 
/

&parameters_control
restart = 0		! initialization mode: 0=new run, 1=restart from checkpoint
runtime = 7200          ! maximum runtime [s] for the job
/

&parameters_initialcondition
ic_tcbf    = T                ! Set Taylor-Couette base flow (T) or resting fluid (F), only when restart = 0
ic_temp    = T                ! Set temperature profile (T) or zero (F), only when restart = 0, only TE_CODE
ic_pert    = T                ! Add perturbation on top of base flow (T) or not (F), only when restart = 0
ic_p(1, :) = 1.0d-2, 0, 2     ! 1st perturbation: amplitude and wavevector (a1, k_th1, k_z1)
ic_p(2, :) = 1.0d-2, 3, 0     ! 2nd perturbation: amplitude and wavevector (a2, k_th2, k_z2)
ic_p(2, :) = 1.0d-2, 3, 0     ! 2nd perturbation: amplitude and wavevector (a2, k_th2, k_z2)
ic_p(3, :) = 0.0d-2, 0, 0     ! 3rd perturbation: amplitude and wavevector (a3, k_th3, k_z3)
ic_p(4, :) = 0.0d-2, 0, 0     ! 4th perturbation: amplitude and wavevector (a4, k_th4, k_z4)
ic_p(5, :) = 0.0d-2, 0, 0     ! 5th perturbation: amplitude and wavevector (a5, k_th5, k_z5)
ic_p(6, :) = 0.0d-2, 0, 0     ! 6th perturbation: amplitude and wavevector (a6, k_th6, k_z6)
/
