#!/usr/bin/env gnuplot
#
# Purpose:  Plot temporal evolution of azimuthallly/axially dependenet kinetic
#           energy for some selected azimuthal/axial modes based on time series
#           output from a nsCouette simulation, nsCouette/tc0081
# Usage:    gnuplot ke_ThZ.gpl
# Authors:  Daniel Feldmann
# Created:  25th August 2018
# Modified: 11th Ocotber 2018

# output
set terminal postscript eps enhanced color font 'Times-Roman,16' linewidth 1.5 size 14.50cm, 06.00cm
file = 'ke_ThZ.eps'
set output file

# title
set title 'Temporal evolution of azimuthally/axially dependent kinetic energy, tc0081'
set notitle 

# 1st x axis
set xlabel 't in d^2/{/Symbol n}'
set format x "%.4f"
set xrange [0.0: 0.0006]
set xtics   0.0, 0.0002
set mxtics  2

# grid lines
set nogrid

# line styles with colours appropriate for colour-blind
set style line 10 lc rgb '#000000' dt 1 lw 1.5 # black
set style line 11 lc rgb '#D55E00' dt 1 lw 1.5 # vermillon
set style line 12 lc rgb '#0072B2' dt 1 lw 1.5 # blue
set style line 13 lc rgb '#009E73' dt 1 lw 1.5 # bluish green
set style line 14 lc rgb '#E69F00' dt 1 lw 1.5 # orange
set style line 15 lc rgb '#56B4E9' dt 1 lw 1.5 # sky blue
set style line 16 lc rgb '#CC79A7' dt 1 lw 1.5 # reddish purple
set style line 17 lc rgb '#F0E442' dt 1 lw 1.5 # yellow 

# main plot
set style data lines
set multiplot layout 1,2

# sub-plot 1
set key top right
set ylabel 'E_{/Symbol q} in d^2/{/Symbol n}^2'
set format y "%.3f"
set yrange [-1.0e-03: 3.0e-02]
set mytics  5 
plot 'ke_th' u ($1):($2) t 'n_{/Symbol q} = 0' ls 10,\
     'ke_th' u ($1):($3) t 'n_{/Symbol q} = 1' ls 11,\
     'ke_th' u ($1):($4) t 'n_{/Symbol q} = 2' ls 12,\
     'ke_th' u ($1):($5) t 'n_{/Symbol q} = 3' ls 13,\
     'ke_th' u ($1):($6) t 'n_{/Symbol q} = 4' ls 14

# sub-plot 2
set key top right
set ylabel 'E_{z} in d^2/{/Symbol n}^2'
plot 'ke_z' u ($1):($2) t 'l_{z} = 0' ls 10,\
     'ke_z' u ($1):($3) t 'l_{z} = 1' ls 11,\
     'ke_z' u ($1):($4) t 'l_{z} = 2' ls 12,\
     'ke_z' u ($1):($5) t 'l_{z} = 3' ls 13,\
     'ke_z' u ($1):($6) t 'l_{z} = 4' ls 14

unset multiplot

# convert to pdf (epstopdf comes with e.g. TeXlive)
system('epstopdf '.file)
