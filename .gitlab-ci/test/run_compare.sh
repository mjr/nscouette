#!/bin/bash

################################################################################
# This file is part of nsCouette -- A high-performance code for direct         #
# numerical simulations of turbulent Taylor-Couette flow                       #
#                                                                              #
# Copyright (C) 2019 Marc Avila, Bjoern Hof, Jose Manuel Lopez, Markus Rampp,  #
#                    Liang Shi, Alberto Vela-Martin, Daniel Feldmann.          #
#                                                                              #
# nsCouette is free software: you can redistribute it and/or modify it under   #
# the terms of the GNU General Public License as published by the Free         #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# nsCouette is distributed in the hope that it will be useful, but WITHOUT ANY #
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS    #
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more        #
# details.                                                                     #
#                                                                              #
# You should have received a copy of the GNU General Public License along with #
# nsCouette. If not, see <http://www.gnu.org/licenses/>.                       #
################################################################################




#run the same setup with different number of MPI tasks and demand identity of 
#all output files
#this can/should be used to challenge the uneven distribution of fourier modes
#and/or different parallelizations

set -e
function exittrap {
    echo "$0 failed"
    exit 1
}
trap exittrap ERR INT TERM

ARCH=$1
NUMPROCS1=${2:-4}
NUMTHRDS1=${3:-1}
NUMPROCS2=${4:-8}
NUMTHRDS2=${5:-1}


export OMP_PLACES=cores
export OMP_STACKSIZE=128M

DIR1=tmpCI_1
DIR2=tmpCI_2
mkdir $DIR1  
mkdir $DIR2  


cd $DIR1
export OMP_NUM_THREADS=$NUMTHRDS1
mpiexec -n $NUMPROCS1 ../$ARCH/nsCouette.x < ../input_nsCouette.test_short 
cd ..

cd $DIR2
export OMP_NUM_THREADS=$NUMTHRDS2
mpiexec -n $NUMPROCS2 ../$ARCH/nsCouette.x < ../input_nsCouette.test_short 
cd ..

FILES="vel_mid ke_mode temp_mid ke_th ke_z ke_total torque Temp_energy Nusselt restart `ls fields_*.xmf coeff_*|xargs`"
for file in $FILES; do
    if [ -e $DIR1/$file ] && [ -e $DIR2/$file ]; then
        echo "@cmp $DIR1/$file $DIR2/$file"
        cmp $DIR1/$file $DIR2/$file
    fi
done

FILES="`ls fields_*.h5|xargs`"
for file in $FILES; do
    if [ -e $DIR1/$file ] && [ -e $DIR2/$file ]; then
        echo "@h5diff $DIR1/$file $DIR2/$file"
        h5diff $DIR1/$file $DIR2/$file
    fi
done

rm -rf $DIR1 $DIR2


  
